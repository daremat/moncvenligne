package fr.takima.demo;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;


import java.io.*;
import java.util.*;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 */
@RequestMapping("/")
@Controller
public class LibraryController {

  private final ProfilDAO profilDAO;
  private final ElementDAO elementDAO;


  public LibraryController(ProfilDAO profilDAO,ElementDAO elementDAO ) {
    this.profilDAO = profilDAO;
    this.elementDAO = elementDAO;
  }

  @GetMapping
  public String homePage(Model m) {

    //si le profil est inexistant dans la db
    if (profilDAO.count()==0) {
      Profil profil = new Profil(); //creation de nouvel objet profil initialisé par default
      profil.setDefaultProfil();
      m.addAttribute("profil", profil);
      return "index"; //affichage de la page html index
    }

    // si un profil existe dans la db, alors on recupere ses données
    m.addAttribute("profil", getProfil());

    //Préparation des listes pour chaque section du CV
      ArrayList<Element> experiences = new ArrayList<>();
      ArrayList<Element> scolarite = new ArrayList<>();
      ArrayList<Element> centredinte = new ArrayList<>();
      ArrayList<Element> competences = new ArrayList<>();

      if(elementDAO.count()!=0){

          Iterable<Element> elements = elementDAO.findAll();

          for (Element element : elements) {
              if (element.getSection().equals("Experience")) {
                  experiences.add(element);
              }

              if (element.getSection().equals("Scolarite")) {
                  scolarite.add(element);
              }

              if (element.getSection().equals("Competence")) {
                  competences.add(element);
              }

              if (element.getSection().equals("Centredinteret")) {
                  centredinte.add(element);
              }
          }

      }
      m.addAttribute("experiences", experiences);
      m.addAttribute("scolarite", scolarite);
      m.addAttribute("competences", competences);
      m.addAttribute("centredinteret", centredinte);
    return "index"; //affichage de la page html index
  }

    @GetMapping("/password")
    public String checkPassword(Model m) {
        m.addAttribute("user", new User());
        return "password";
    }

    @PostMapping("/password")
    public RedirectView checkPassword(@ModelAttribute User user ) {
        if(user.getPassword().equals("toor")){ //definition du mot de passe
            return new RedirectView("/modifier");
        }
        else
            return new RedirectView("/password");
    }

  @GetMapping("/modifier")
  public String modifyCV(Model m) {

    if (profilDAO.count()==0) {
      Profil profil = new Profil();
      profil.setDefaultProfil();
      m.addAttribute("profil", profil);

    }
    else
      m.addAttribute("profil", getProfil());

      ArrayList<Element> experiences = new ArrayList<>();
      ArrayList<Element> scolarite = new ArrayList<>();
      ArrayList<Element> centredinte = new ArrayList<>();
      ArrayList<Element> competences = new ArrayList<>();

    if(elementDAO.count()!=0){
        Iterable<Element> elements = elementDAO.findAll();

        for (Element element : elements) {
            if (element.getSection().equals("Experience")) {
                experiences.add(element);
            }

            if (element.getSection().equals("Scolarite")) {
                scolarite.add(element);
            }

            if (element.getSection().equals("Competence")) {
                competences.add(element);
            }

            if (element.getSection().equals("Centredinteret")) {
                centredinte.add(element);
            }
        }

    }
      m.addAttribute("experiences", experiences);
      m.addAttribute("scolarite", scolarite);
      m.addAttribute("competences", competences);
      m.addAttribute("centredinteret", centredinte);
   return "modifier";

  }

    @GetMapping("/modifier/profil")
    public String changeProfil(Model m) {
        if(profilDAO.count() == 0){
            m.addAttribute("profil", new Profil());
            return "profil";
        }
        m.addAttribute("profil", getProfil());
        return "profil";
    }

    @PostMapping("/modifier/profil")
    public RedirectView changeProfil(@ModelAttribute Profil profil, RedirectAttributes attrs) {
        if(profilDAO.count() == 0){
            profilDAO.save(profil);
            return new RedirectView("/modifier");
        }
        profilDAO.deleteAll();
        profilDAO.save(profil);
        return new RedirectView("/modifier");

    }


  private Profil getProfil() {
    Iterable<Profil> profils = profilDAO.findAll();
    return profils.iterator().next();
  }



  @GetMapping(value="/modifier/element")
  public String changeElement(@ModelAttribute Element element, Model m, @RequestParam(value="editButton") int id) {
    if(elementDAO.count() == 0){
      m.addAttribute("element", new Element());
      return "element";
    }
    m.addAttribute("element", getElement(id));
    return "element";


  }
  private Element getElement(long id) {
    Optional<Element> optionnalElement = elementDAO.findById(id);

    return optionnalElement.get();
  }


  @PostMapping("/modifier/element")
  public RedirectView changeElement(@ModelAttribute Element elementNouveau, @RequestParam(value="saveButton") long id) {
    Optional<Element> optionnalElement = elementDAO.findById(id);
    Element ancienElement= optionnalElement.get();
    elementNouveau.setSection(ancienElement.getSection());

    elementDAO.delete(ancienElement);

    elementDAO.save(elementNouveau);
    return new RedirectView("/modifier");
  }




    @GetMapping("/modifier/addelement")
    public String addelement(Model m, @RequestParam(value= "addbutton1") String section) {
      Element element=new Element();
        element.setSection(section);
        m.addAttribute("element", element);

        return "addelement";
    }

    @PostMapping("/modifier/addelement")
    public RedirectView addelement(@ModelAttribute Element element, RedirectAttributes attrs, @RequestParam(value= "saveButton") String section) {
      element.setSection(section);
      elementDAO.save(element);
        return new RedirectView("/modifier");
    }



    @PostMapping("/modifier/delete")
    public RedirectView deleteElement(@RequestParam(value = "deleteButton") int idElement ) {
        Optional<Element> optionnalElement = elementDAO.findById(Long.valueOf(idElement)); //trouver une valeure optionnelle dans la base de donnée Element grace à son ID
        elementDAO.delete(optionnalElement.get());
        return new RedirectView("/modifier");
    }


    // generate PDF
    @Autowired
    private HtmlService pdfService;

    @GetMapping("/pdf")
    public RedirectView pdf() throws IOException{

        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML");

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);


        // Get the plain HTML with the resolved ${name} variable!
        String renderHtmlContent = pdfService.template2Html("index");

        OutputStream outputStream = new FileOutputStream("cv.pdf");

        ConverterProperties converterProperties = new ConverterProperties();
        HtmlConverter.convertToPdf(renderHtmlContent,
                outputStream, converterProperties);

        return new RedirectView("/");

    }

}
