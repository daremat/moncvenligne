package fr.takima.demo;

import com.itextpdf.io.source.OutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.ArrayList;
import java.util.Map;

@Service
public class HtmlService {

    @Autowired
    private TemplateEngine templateEngine;
    private final ProfilDAO profilDAO;
    private final ElementDAO elementDAO;


    public HtmlService(ProfilDAO profilDAO, ElementDAO elementDAO) {
        this.profilDAO = profilDAO;
        this.elementDAO = elementDAO;
    }

    public String template2Html(String templateName) {
        Context ctx = new Context();
        if (profilDAO.count() != 0) {
            ctx.setVariable("profil", getProfil());
        }
        if (elementDAO.count() != 0) {
            getElements(ctx);
        }

        String processedHtml = templateEngine.process(templateName, ctx);
        return processedHtml;
    }

    private void getElements(Context ctx) {
        ArrayList<Element> experiences = new ArrayList<>();
        ArrayList<Element> scolarite = new ArrayList<>();
        ArrayList<Element> centredinte = new ArrayList<>();
        ArrayList<Element> competences = new ArrayList<>();

        Iterable<Element> elements = elementDAO.findAll();

        for (Element element : elements) {
            if (element.getSection().equals("Experience")) {
                experiences.add(element);
            }

            if (element.getSection().equals("Scolarite")) {
                scolarite.add(element);
            }

            if (element.getSection().equals("Competence")) {
                competences.add(element);
            }

            if (element.getSection().equals("Centredinteret")) {
                centredinte.add(element);
            }
        }
        ctx.setVariable("experiences", experiences);
        ctx.setVariable("scolarite", scolarite);
        ctx.setVariable("competences", competences);
        ctx.setVariable("centredinteret", centredinte);
    }

    private Profil getProfil() {
        Iterable<Profil> profils = profilDAO.findAll();
        return profils.iterator().next();
    }


}