package fr.takima.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Element {

    String titre;
    String datedebut;
    String datefin;
    String entreprise;
    String description;
    @Id
    @GeneratedValue
    long id;
    String section;

    public Element(String titre, String datedebut, String datefin, String entreprise, String description, String section) {
        this.titre = titre;
        this.datedebut = datedebut;
        this.datefin = datefin;
        this.entreprise = entreprise;
        this.description = description;
        this.section = section;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(String datedebut) {
        this.datedebut = datedebut;
    }

    public String getDatefin() {
        return datefin;
    }

    public void setDatefin(String datefin) {
        this.datefin = datefin;
    }

    public String getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Element element = (Element) o;
        return id == element.id &&
                titre.equals(element.titre) &&
                datedebut.equals(element.datedebut) &&
                datefin.equals(element.datefin) &&
                entreprise.equals(element.entreprise) &&
                description.equals(element.description) &&
                section.equals(element.section);
    }

    @Override
    public int hashCode() {
        return Objects.hash(titre, datedebut, datefin, entreprise, description, id, section);
    }


    @Override
    public String toString() {
        return "Element{" +
                "titre='" + titre + '\'' +
                ", datedebut='" + datedebut + '\'' +
                ", datefin='" + datefin + '\'' +
                ", entreprise='" + entreprise + '\'' +
                ", description='" + description + '\'' +
                ", id=" + id +
                ", section=" + section +
                '}';
    }

    public void setDefaultElement() {
        this.titre = "Titre";
        this.datedebut = "Date debut";
        this.datefin = "Date de fin";
        this.entreprise = "Entreprise";
        this.description = "Description";
        this.section = "Expérience";
    }

    public Element() {
        this.titre = null;
        this.datedebut = null;
        this.datefin = null;
        this.entreprise = null;
        this.description = null;
    }
}
