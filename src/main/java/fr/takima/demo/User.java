package fr.takima.demo;

public class User{
    String password;

    public User() {
        this.password = null;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}