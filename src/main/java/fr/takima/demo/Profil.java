package fr.takima.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Profil {

    String fullname;
    String post;
    String number;
    String mail;
    String adresse;

    @Id
    @GeneratedValue
    Long id;


    public Profil(String fullname, String poste, String numero, String mail, String adresse) {
        this.fullname = fullname;
        this.post = poste;
        this.number = numero;
        this.mail = mail;
        this.adresse = adresse;
    }

    public Profil() {
        this.fullname = null;
        this.post = null;
        this.number = null;
        this.mail = null;
        this.adresse = null;
    }
    
    public void setDefaultProfil(){
        this.fullname = "votre nom et prenom";
        this.post = "votre poste";
        this.number = "votre numero de telephone";
        this.mail = "votre mail";
        this.adresse = "votre adresse";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profil profil = (Profil) o;
        return fullname.equals(profil.fullname) &&
                post.equals(profil.post) &&
                number.equals(profil.number) &&
                mail.equals(profil.mail) &&
                adresse.equals(profil.adresse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullname, post, number, mail, adresse);
    }

    @Override
    public String toString() {
        return "Profil{" +
                "Nom='" + fullname + '\'' +
                ", poste='" + post + '\'' +
                ", numero='" + number + '\'' +
                ", mail='" + mail + '\'' +
                ", adresse='" + adresse + '\'' +
                '}';
    }
}
